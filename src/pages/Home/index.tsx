import { FormEvent, useState } from "react";
import { useHistory } from "react-router-dom";

import { Button } from "../../components/Button";
import { useAuth } from "../../hooks/useAuth";
import { database } from "../../services/firebase";

import illustrationImg from "../../assets/images/illustration.svg";
import logoImg from "../../assets/images/logo.svg";
import googleIconImg from "../../assets/images/google-icon.svg";

import "../../styles/auth.scss";

export function Home() {
  const history = useHistory();
  const [roomId, setRoomId] = useState("");
  const { user, loginWithGoogle } = useAuth();

  async function handleCreateRoom() {
    if (!user) {
      await loginWithGoogle();
    }

    history.push("/rooms/create");
  }

  async function handleJoinRoom(event: FormEvent) {
    event.preventDefault();

    if (roomId.trim() === "") {
      return;
    }

    const roomRef = await database.ref(`rooms/${roomId}`).get();

    if (!roomRef.exists()) {
      alert("Esta sala não existe, favor verifiquei o código digitado");
      return;
    }

    if (roomRef.val().endedAt) {
      alert("Esta sala foi encerrada. Tente outra.");
      return;
    }

    history.push(`/rooms/${roomId}`);
  }

  return (
    <div id="page-auth">
      <aside>
        <img src={illustrationImg} alt="Ilustração perguntas e respostas" />
        <strong>Crie salas de Q&A ao-vivo</strong>
        <p>Tire as dúvidas da sua audiência em tempo-real</p>
      </aside>
      <main>
        <div className="main-content">
          <img src={logoImg} alt="Letmeask" />
          <button onClick={handleCreateRoom} className="btn-create-room">
            <img src={googleIconImg} alt="Logo Google" />
            Crie sua sala com o Google
          </button>
          <div className="separator">ou entre em uma sala</div>
          <form onSubmit={handleJoinRoom}>
            <input
              type="text"
              value={roomId}
              placeholder="Digite o código da sala"
              onChange={(event) => setRoomId(event.target.value)}
            />
            <Button type="submit">Entrar na sala</Button>
          </form>
        </div>
      </main>
    </div>
  );
}
