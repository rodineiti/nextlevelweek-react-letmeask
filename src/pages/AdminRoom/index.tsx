import { useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import { Button } from "../../components/Button";
import { RoomCode } from "../../components/RoomCode";
import { Question } from "../../components/Question";
import { Modal } from "../../components/Modal";
import { useRoom } from "../../hooks/useRoom";
import { database } from "../../services/firebase";

import logoImg from "../../assets/images/logo.svg";
import deleteImg from "../../assets/images/delete.svg";
import answerImg from "../../assets/images/answer.svg";
import checkImg from "../../assets/images/check.svg";

import "../../styles/room.scss";

type RoomParams = {
  id: string;
};

export function AdminRoom() {
  const history = useHistory();
  const [open, setOpen] = useState(false);
  const [itemId, setItemId] = useState("");
  const [titleModal, setTitleModal] = useState("");
  const [subtitle, setSubtitle] = useState("");
  const params = useParams<RoomParams>();
  const roomId = params.id;
  const { questions, title } = useRoom(roomId);

  async function onEndRoom() {
    if (window.confirm("Tem certeza que você deseja encerrar esta sala?")) {
      await database.ref(`rooms/${roomId}`).update({
        endedAt: new Date()
      });

      history.push("/");
    }
  }

  async function onDeleteQuestion(questionId: string) {
    if (questionId) {
      await database.ref(`rooms/${roomId}/questions/${questionId}`).remove();
    }
    setOpen(false);
  }

  async function onCheckQuestionAsAnswered(questionId: string) {
    await database.ref(`rooms/${roomId}/questions/${questionId}`).update({
      isAnswered: true
    });
  }

  async function onHighlithQuestion(questionId: string) {
    await database.ref(`rooms/${roomId}/questions/${questionId}`).update({
      isHighlighted: true
    });
  }

  function onModal(questionId: string, title: string, subtitle: string) {
    if (questionId) {
      setItemId(questionId);
      setTitleModal(title);
      setSubtitle(subtitle);
      setOpen(true);
    }
  }

  return (
    <div id="page-room">
      <header>
        <div className="content">
          <img src={logoImg} alt="Letmeask" />
          <div>
            <RoomCode code={roomId} />
            <Button isOutlined onClick={onEndRoom}>
              Encerrar sala
            </Button>
          </div>
        </div>
      </header>

      <main>
        <div className="room-title">
          <h1>Sala: {title}</h1>
          {questions.length > 0 && (
            <span>
              {questions.length}{" "}
              {`${questions.length > 1 ? "Perguntas" : "Pergunta"}`}
            </span>
          )}
        </div>

        <div className="question-list">
          {questions.length > 0 &&
            questions.map((item) => {
              return (
                <Question
                  key={item.id}
                  content={item.content}
                  author={item.author}
                  isAnswered={item.isAnswered}
                  isHighlighted={item.isHighlighted}
                >
                  {!item.isAnswered && (
                    <>
                      <button
                        type="button"
                        onClick={() => onCheckQuestionAsAnswered(item.id)}
                      >
                        <img src={checkImg} alt="Marcar como respondida" />
                      </button>
                      <button
                        type="button"
                        onClick={() => onHighlithQuestion(item.id)}
                      >
                        <img src={answerImg} alt="Destacar pergunta" />
                      </button>
                    </>
                  )}
                  <button
                    type="button"
                    onClick={() =>
                      onModal(
                        item.id,
                        "Excluir pergunta",
                        "Tem certeza que você deseja excluir a pergunta?"
                      )
                    }
                  >
                    <img src={deleteImg} alt="Remover pergunta" />
                  </button>
                </Question>
              );
            })}
        </div>

        {open && (
          <Modal
            open={open}
            title={titleModal}
            subtitle={subtitle}
            handleClose={() => setOpen(false)}
            handleClick={() => onDeleteQuestion(itemId)}
          />
        )}
      </main>
    </div>
  );
}
