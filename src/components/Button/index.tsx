import { ButtonHTMLAttributes } from "react";

type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  isOutlined?: boolean;
};

import "../../styles/button.scss";

export function Button({ isOutlined = false, ...props }: ButtonProps) {
  return (
    <button className={`btn ${isOutlined ? "outlined" : ""}`} {...props} />
  );
}
